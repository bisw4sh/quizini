import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Route,
} from "react-router-dom";
import HomePage from "./pages/HomePage";
import Error from "./pages/Error";
import Navbar from "./components/Navbar";
import MainPage, {
  loader as quizLoader,
  action as quizAction,
} from "./pages/MainPage";
import Options from "./components/Options";

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<Navbar />} errorElement={<Error />}>
        <Route index element={<HomePage />} />
        <Route path="/main" element={<Options />}>
          <Route
            path=":fileName"
            element={<MainPage />}
            loader={quizLoader}
            action={quizAction}
            shouldRevalidate={() => false}
            index
          ></Route>
        </Route>
      </Route>
    </>,
  ),
);

export default function App() {
  return <RouterProvider router={router} fallbackElement={<Error />} />;
}
